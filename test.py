from configuration import Config
from ner_tagger import NERModel
from utils import CoNLLDataset
# create instance of config
config = Config()

# build model
model = NERModel(config)
model.build()
model.restore_session(config.dir_model)

# create dataset
test = CoNLLDataset(config.filename_test, config.processing_word, config.processing_pos,
                    config.processing_tags, config.max_iter)

# evaluate and interact
model.evaluate(test)