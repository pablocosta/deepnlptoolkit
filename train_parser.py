import build_data_parser
# lmbrar que se executarmos o build data devemos trocar a ordem do pad e do eos no arquivos words.txt
import tensorflow as tf
from DependencyTree import ReadTrees
from configuration_parser import Config
from tensorflow.contrib.rnn import LSTMCell, GRUCell
from base_model_parser import Seq2SeqModel
from utils import get_vocabs, NONE, NUM, EOS, BOS, PAD, UNK, ROOT, NULL, \
    write_vocab, get_char_vocab, \
    get_processing_word, get_embeddings

#create instance of config
config = Config(load=True, using_bid=True)
#camada do modelo seq2seq for UAS dependecy parsing

(trees_train, sents_train) = ReadTrees(config.filename_train, processing_word=config.processing_word, processing_pos=config.processing_pos, processing_tag=config.processing_tags).load_corpus()
(trees_dev, sents_dev)     = ReadTrees(config.filename_dev,   processing_word=config.processing_word, processing_pos=config.processing_pos, processing_tag=config.processing_tags).load_corpus()

tf.reset_default_graph()
tf.set_random_seed(1)

with tf.Session() as session:
    # build model
    model = Seq2SeqModel(config,
                         encoder_cell=LSTMCell(config.hidden_size_encoder),
                         decoder_cell=LSTMCell(config.hidden_size_decoder),
                         vocab_size=config.nwords,
                         embedding_size=config.dim_word,
                         attention=True,
                         bidirectional=True,
                         attention_form="bahdanau",
                         debug=False)
    session.run(tf.global_variables_initializer())
    loss_tracks['bidirectional lstm, with bahdanau attention'] = model.train((trees_train, sents_train), (trees_dev, sents_dev), session, model)