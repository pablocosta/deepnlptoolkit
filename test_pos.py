from configuration import Config
from pos_tagger import POSModel
import os
from utils import UniversalDependencies
# create instance of config
config = Config()

# build model
model = POSModel(config)
model.build()
model.restore_session(config.dir_model)

# create dataset
test    = UniversalDependencies(config.filename_test, config.processing_word, config.processing_case, config.processing_pos, config.processing_tags, config.max_iter)
# evaluate and interact
model.evaluate(test)

