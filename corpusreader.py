from utils import get_processing_word

class Sent():
    n = 0
    words = []
    pos = []
    def __init__(self):
        self.n = 0
        self.words = []
        self.pos = []
        self.label = []
    def add(self, word, pos, label):
        self.n += 1
        self.words.append(word)
        self.pos.append(pos)
        self.label.append(label)


class ReadSent():
    def __init__(self, path, processing_word, processing_pos, processing_label):
        self.path = path
        self.processing_word = processing_word
        self.processing_pos = processing_pos
        self.processing_label = processing_label
        self.sents = self.load_corpus()
    def load_corpus(self):
        i       = 0
        sents   = []
        #sent    = Sent()
        arquivo = open(self.path, encoding="utf-8")
        word  = list()
        pos   = list()
        label = list()

        vocab_words = set()
        #vocab_char = set()
        vocab_pos = set()
        vocab_label = set()


        for line in arquivo:
            line = line.rstrip()
            fields = line.split("\t")
            if line is "":
                continue
                #sent = Sent()
            elif line.startswith("-DOCSTART-") or line.startswith("#"):
                i += 1
                continue
            else:
                vocab_pos.add(fields[3])
                vocab_label.add(fields[7])
                vocab_words.add(fields[2])

        vocab_pos.add("UNK")
        vocab_words.add("UNK")
        arquivo.close()
        self.vocab_label = self.load_vocab(vocab_label)
        self.vocab_pos   = self.load_vocab(vocab_pos)
        self.vocab_char  = self.load_vocab(self.load_charvocab(vocab_words))
        self.vocab_words = self.load_vocab(vocab_words)
        self.size        = i
        return ""

    def load_charvocab(self,  vocab):
        vocab_char = set()
        for word in vocab:
            for letter in word:
                vocab_char.add(str(letter))
        #print(self.vocab_char)
        return vocab_char

    def load_vocab(self, set):
        """transform a set into a dict
        Args:
            set: (string) of the vocab.
        Returns:
            d: dict[word] = index
        """
        d = dict()
        for idx, word in enumerate(set):
            word = word.strip()
            d[word] = idx

        return d




#ReadSent("./data_set/train.txt").load_corpus()