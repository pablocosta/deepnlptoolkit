# -*- coding: utf-8 -*-
from configuration import Config
from utils import UniversalDependencies, get_vocabs, UNK, NUM, NONE, \
    write_vocab, load_vocab, get_char_vocab, \
    get_processing_word, get_embeddings, transform_embeddings, create_dict_inx


# -*- coding: utf-8 -*-

# get config and processing of words
config = Config(load=False)

processing_word = get_processing_word(lowercase=True)
#processing_cases = get_processing_word(lowercase=False, allow_unk=True)

# Generators
dev   = UniversalDependencies(config.filename_dev, processing_word)
test  = UniversalDependencies(config.filename_test, processing_word)
train = UniversalDependencies(config.filename_train, processing_word)

# Build Word and Tag vocab
vocab_words,vocab_pos, vocab_tags = get_vocabs([train, dev, test])
vocab_glove = get_embeddings("en_50.txt")

vocab_pos.add(UNK)
vocab = vocab_words & set(vocab_glove.keys())
vocab.add(UNK)
vocab.add(NUM)
vocab_tags.add(NONE)
# Save vocab
write_vocab(vocab, config.filename_words)
write_vocab(vocab_pos, config.filename_pos)
write_vocab(vocab_tags, config.filename_tags)

# Trim GloVe Vectors
#vocab = load_vocab(config.filename_words)
#export_trimmed_glove_vectors(vocab, config.filename_glove,
                            #config.filename_trimmed, config.dim_word)

# Build and save char vocab
train = UniversalDependencies(config.filename_train)
vocab_chars = get_char_vocab(train)
write_vocab(vocab_chars, config.filename_chars)