
from configuration import Config
from utils import CoNLLDataset, get_vocabs, UNK, NUM,NONE, \
    write_vocab, load_vocab, get_char_vocab, \
    get_processing_word, get_embeddings
# -*- coding: utf-8 -*-

# get config and processing of words
config = Config(load=False)

processing_word = get_processing_word(lowercase=True)
#processing_cases = get_processing_word(lowercase=False, allow_unk=True)

# Generators
dev   = CoNLLDataset(config.filename_dev, processing_word)
test  = CoNLLDataset(config.filename_test, processing_word)
train = CoNLLDataset(config.filename_train, processing_word)

# Build Word and Tag vocab
vocab_words,vocab_pos, vocab_tags = get_vocabs([train, dev, test])
vocab_glove = get_embeddings("pt_50.txt", 50)

vocab_pos.add(UNK)
vocab = vocab_words & set(vocab_glove.keys())
vocab.add(UNK)
vocab.add(NUM)
# Save vocab
write_vocab(vocab, config.filename_words)
write_vocab(vocab_pos, config.filename_pos)
write_vocab(vocab_tags, config.filename_tags)


# Build and save char vocab
train = CoNLLDataset(config.filename_train)
vocab_chars = get_char_vocab(train)
write_vocab(vocab_chars, config.filename_chars)