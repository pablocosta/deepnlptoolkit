import logging
import sys
import time
import numpy as np
import pickle
import re
from os import path
UNK = "unk"
NUM = "num"
NONE = "o"
PAD = "pad"
EOS = "eos"
BOS = "bos"
ROOT = "root"
NULL = "null"

# -*- coding: utf-8 -*-
# special error message
class MyIOError(Exception):
    def __init__(self, filename):
        # custom error message
        message = """
ERROR: Unable to locate file {}.
FIX: Have you tried running python build_data.py first?
This will build vocab file from your train, test and dev sets and
trimm your word vectors.
""".format(filename)
        super(MyIOError, self).__init__(message)

def pad_sentences(sentences, forced_sequence_length=None, config=None, encoder=True):
    """Pad setences during training or prediction"""
    sequence_lengths = [len(seq) for seq in sentences]
    if forced_sequence_length is None:  # Train
        sequence_length = max(len(x) for x in sentences)
    # sequence_length = 20
    else:  # Prediction
        logging.critical('This is prediction, reading the trained sequence length')
        sequence_length = forced_sequence_length

    logging.critical('The maximum length is {}'.format(sequence_length))

    padded_sentences = []
    for i in range(len(sentences)):
        sentence = sentences[i]
        num_padding = sequence_length - len(sentence)

        if num_padding < 0:  # Prediction: cut off the sentence if it is longer than the sequence length
            padded_sentence = sentence[0:sequence_length]
        else:

            if encoder:
                padded_sentence = sentence + [config.vocab_words[PAD]] * num_padding
            else:
                padded_sentence = sentence + [config.vocab_words[PAD]] * num_padding

        padded_sentences.append(padded_sentence)
    return padded_sentences, sequence_lengths



def _pad_sequences(sequences, pad_tok, max_length):
    """
    Args:
        sequences: a generator of list or tuple
        pad_tok: the char to pad with
    Returns:
        a list of list where each sublist has same length
    """
    sequence_padded, sequence_length = [], []

    for seq in sequences:
        seq = list(seq)
        seq_ = seq[:max_length] + [pad_tok]*max(max_length - len(seq), 0)
        sequence_padded +=  [seq_]
        sequence_length += [min(len(seq), max_length)]

    return sequence_padded, sequence_length
def create_dict_inx(dictionary):
    return_dict = dict()
    i = 0
    for key in dictionary.keys():
        return_dict[key] = i
        i += 1
    return return_dict


def get_embeddings(file_embedding_model, emb_size):
    arquivo = open(path.join("./embeddings/", file_embedding_model), encoding="utf8")

    embeddings = dict()
    for s in arquivo:
        s = s.rstrip().split(" ")
        if s[0] not in embeddings.keys():
            embeddings[s[0]] = [float(x) for x in s[1:]]
    #fix-it mannualy sized :(
    embeddings["UNK"]  = np.random.uniform(low=-0.01, high=0.01, size=emb_size)
    embeddings["NUM"]  = np.random.uniform(low=-0.01, high=0.01, size=emb_size)
    embeddings["NONE"] = np.random.uniform(low=-0.01, high=0.01, size=emb_size)

    arquivo.close()
    return embeddings

def transform_embeddings(dict_vec, dict_indx, size):
    weights = np.zeros((len(dict_vec.keys()) + 1, size))
    for key in dict_indx.keys():
        if key in dict_vec.keys():
            weights[dict_indx[key], :] = dict_vec[key]


    return weights

def set_casing(word):
    casing = 'other'

    numDigits = 0
    for char in word:
        if char.isdigit():
            numDigits += 1

    digitFraction = numDigits / float(len(word))

    if word.isdigit(): #Is a digit
        casing = 'numeric'
    elif digitFraction > 0.5:
        casing = 'mainly_numeric'
    elif word.islower(): #All lower case
        casing = 'allLower'
    elif word.isupper(): #All upper case
        casing = 'allUpper'
    elif word[0].isupper(): #is a title, initial char upper, then all lower
        casing = 'initialUpper'
    elif numDigits > 0:
        casing = 'contains_digit'


    return casing

def pad_sequences(sequences, pad_tok, nlevels=1):
    """
    Args:
        sequences: a generator of list or tuple
        pad_tok: the char to pad with
        nlevels: "depth" of padding, for the case where we have characters ids
    Returns:
        a list of list where each sublist has same length
    """
    if nlevels == 1:
        max_length = max(map(lambda x : len(x), sequences))
        sequence_padded, sequence_length = _pad_sequences(sequences,
                                            pad_tok, max_length)

    elif nlevels == 2:
        max_length_word = max([max(map(lambda x: len(x), seq))
             for seq in sequences])
        sequence_padded, sequence_length = [], []
        for seq in sequences:
            # all words are same length now
            sp, sl = _pad_sequences(seq, pad_tok, max_length_word)
            sequence_padded += [sp]
            sequence_length += [sl]

        max_length_sentence = max(map(lambda x : len(x), sequences))
        sequence_padded, _ = _pad_sequences(sequence_padded,
                                            [pad_tok] * max_length_word, max_length_sentence)
        sequence_length, _ = _pad_sequences(sequence_length, 0,
                                            max_length_sentence)

    return sequence_padded, sequence_length

def pad_sequences_n_1(sequences, pad_tok, nlevels=1, max_length=0):
    """
    Args:
        sequences: a generator of list or tuple
        pad_tok: the char to pad with
        nlevels: "depth" of padding, for the case where we have characters ids
    Returns:
        a list of list where each sublist has same length
    """
    if nlevels == 1:
        max_length = max(max_length)
        sequence_padded, sequence_length = _pad_sequences(sequences, pad_tok, max_length)

    elif nlevels == 2:
        #max_length_word = max([max(map(lambda x: len(x), seq))
             #for seq in sequences])
        sequence_padded, sequence_length = [], []
        for seq in sequences:
            # all words are same length now
            sp, sl = _pad_sequences(seq, pad_tok, max_length_word)
            sequence_padded += [sp]
            sequence_length += [sl]

        max_length_sentence = max(map(lambda x : len(x), sequences))
        sequence_padded, _ = _pad_sequences(sequence_padded,
                                            [pad_tok] * max_length_word, max_length_sentence)
        sequence_length, _ = _pad_sequences(sequence_length, 0,
                                            max_length_sentence)

    return sequence_padded, sequence_length



def minibatches(data, minibatch_size):
    """
    Args:
        data: generator of (sentence, pos, label) tuples
        minibatch_size: (int)
    Yields:
        list of tuples
    """
    x_batch, p_batch,c_batch, y_batch = [], [], [], []
    for (x,p,c, y) in data:
    #for (x, y) in data:
        if len(x_batch) == minibatch_size:
            yield x_batch, p_batch,c_batch, y_batch
            #yield x_batch, y_batch
            x_batch, p_batch, c_batch, y_batch = [], [], [], []

        if type(x[0]) == tuple:
            x = zip(*x)
        x_batch += [x]
        p_batch += [p]
        c_batch += [c]
        y_batch += [y]

    if len(x_batch) != 0:
        yield x_batch, p_batch,c_batch, y_batch
        #yield x_batch, y_batch

def minibatches_sentiment(data, minibatch_size):
    """
    Args:
        data: generator of (text, label) tuples
        minibatch_size: (int)
    Yields:
        list of tuples
    """
    x_batch, y_batch = [], []
    for (x, y) in data:
    #for (x, y) in data:
        if len(x_batch) == minibatch_size:
            yield x_batch, y_batch
            #yield x_batch, y_batch
            x_batch, y_batch = [], []

        if type(x[0]) == tuple:
            x = zip(*x)
        x_batch += [x]
        y_batch += [y]

    if len(x_batch) != 0:
        yield x_batch, y_batch
        #yield x_batch, y_batch


def get_processing_word(vocab_words=None, vocab_chars=None,
                    lowercase=False, chars=False, allow_unk=True):
    """Return lambda function that transform a word (string) into list,
    or tuple of (list, id) of int corresponding to the ids of the word and
    its corresponding characters.
    Args:
        vocab: dict[word] = idx
    Returns:
        f("cat") = ([12, 4, 32], 12345)
                 = (list of char ids, word id)
    """
    def f(word):
        # 0. get chars of words
        if vocab_chars is not None and chars == True:
            char_ids = []
            for char in word:
                # ignore chars out of vocabulary
                if char in vocab_chars:
                    char_ids += [vocab_chars[char]]

        # 1. preprocess word
        if lowercase:
            word = word.lower()

        # 2. get id of word
        if vocab_words is not None:
            if word in vocab_words:
                word = vocab_words[word]
            else:
                if allow_unk:
                    word = vocab_words[UNK]
                else:
                    raise Exception("Unknow key is not allowed. Check that "\
                                    "your vocab (tags?) is correct")

        # 3. return tuple char ids, word id
        if vocab_chars is not None and chars == True:
            return char_ids, word
        else:
            return word

    return f

def get_logger(filename):
    """Return a logger instance that writes in filename
    Args:
        filename: (string) path to log.txt
    Returns:
        logger: (instance of logger)
    """
    logger = logging.getLogger('logger')
    logger.setLevel(logging.DEBUG)
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)
    handler = logging.FileHandler(filename)
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(logging.Formatter(
        '%(asctime)s:%(levelname)s: %(message)s'))
    logging.getLogger().addHandler(handler)

    return logger



class Progbar(object):
    """Progbar class copied from keras (https://github.com/fchollet/keras/)
    Displays a progress bar.
    Small edit : added strict arg to update
    # Arguments
        target: Total number of steps expected.
        interval: Minimum visual progress update interval (in seconds).
    """

    def __init__(self, target, width=30, verbose=1):
        self.width = width
        self.target = target
        self.sum_values = {}
        self.unique_values = []
        self.start = time.time()
        self.total_width = 0
        self.seen_so_far = 0
        self.verbose = verbose

    def update(self, current, values=[], exact=[], strict=[]):
        """
        Updates the progress bar.
        # Arguments
            current: Index of current step.
            values: List of tuples (name, value_for_last_step).
                The progress bar will display averages for these values.
            exact: List of tuples (name, value_for_last_step).
                The progress bar will display these values directly.
        """

        for k, v in values:
            if k not in self.sum_values:
                self.sum_values[k] = [v * (current - self.seen_so_far),
                                      current - self.seen_so_far]
                self.unique_values.append(k)
            else:
                self.sum_values[k][0] += v * (current - self.seen_so_far)
                self.sum_values[k][1] += (current - self.seen_so_far)
        for k, v in exact:
            if k not in self.sum_values:
                self.unique_values.append(k)
            self.sum_values[k] = [v, 1]

        for k, v in strict:
            if k not in self.sum_values:
                self.unique_values.append(k)
            self.sum_values[k] = v

        self.seen_so_far = current

        now = time.time()
        if self.verbose == 1:
            prev_total_width = self.total_width
            sys.stdout.write("\b" * prev_total_width)
            sys.stdout.write("\r")

            numdigits = int(np.floor(np.log10(self.target))) + 1
            barstr = '%%%dd/%%%dd [' % (numdigits, numdigits)
            bar = barstr % (current, self.target)
            prog = float(current)/self.target
            prog_width = int(self.width*prog)
            if prog_width > 0:
                bar += ('='*(prog_width-1))
                if current < self.target:
                    bar += '>'
                else:
                    bar += '='
            bar += ('.'*(self.width-prog_width))
            bar += ']'
            sys.stdout.write(bar)
            self.total_width = len(bar)

            if current:
                time_per_unit = (now - self.start) / current
            else:
                time_per_unit = 0
            eta = time_per_unit*(self.target - current)
            info = ''
            if current < self.target:
                info += ' - ETA: %ds' % eta
            else:
                info += ' - %ds' % (now - self.start)
            for k in self.unique_values:
                if type(self.sum_values[k]) is list:
                    info += ' - %s: %.4f' % (k,
                        self.sum_values[k][0] / max(1, self.sum_values[k][1]))
                else:
                    info += ' - %s: %s' % (k, self.sum_values[k])

            self.total_width += len(info)
            if prev_total_width > self.total_width:
                info += ((prev_total_width-self.total_width) * " ")

            sys.stdout.write(info)
            sys.stdout.flush()

            if current >= self.target:
                sys.stdout.write("\n")

        if self.verbose == 2:
            if current >= self.target:
                info = '%ds' % (now - self.start)
                for k in self.unique_values:
                    info += ' - %s: %.4f' % (k,
                        self.sum_values[k][0] / max(1, self.sum_values[k][1]))
                sys.stdout.write(info + "\n")

    def add(self, n, values=[]):
        self.update(self.seen_so_far+n, values)



class CoNLLDataset(object):
    """Class that iterates over CoNLL Dataset
    __iter__ method yields a tuple (words, tags)
        words: list of raw words
        tags: list of raw tags
    If processing_word and processing_tag are not None,
    optional preprocessing is appplied
    Example:
        ```python
        data = CoNLLDataset(filename)
        for sentence, pos, labels in data:
            pass
        ```
    """
    def __init__(self, filename, processing_word=None, processing_cases=None, processing_pos=None, processing_tag=None,
                 max_iter=None):
        """
        Args:
            filename: path to the file
            processing_words: (optional) function that takes a word as input
            processing_tags: (optional) function that takes a tag as input
            max_iter: (optional) max number of sentences to yield
        """
        self.filename = filename
        self.processing_word  = processing_word
        self.processing_tag   = processing_tag
        self.processing_pos   = processing_pos
        self.processing_cases = processing_cases
        self.max_iter = max_iter
        self.length = None


    def __iter__(self):
        niter = 0
        print(self.filename)
        with open(self.filename, encoding="utf-8") as f:
            words, poss, tags, cases = [], [], [], []
            for line in f:
                line = line.strip()
                if (len(line) == 0 or line.startswith("-DOCSTART-") or line.startswith("#")):
                    if len(words) != 0:
                        niter += 1
                        if self.max_iter is not None and niter > self.max_iter:
                            break
                        yield words, poss, cases, tags
                        #yield words, tags
                        words, poss, tags, cases = [], [], [], []
                else:
                    ls = line.split('\t')
                    word, pos, tag = ls[0], ls[1], ls[-1]

                    if self.processing_cases is not None:
                        case = self.processing_cases(set_casing(word))
                    else:
                        case = []
                    if self.processing_word is not None:
                        word = self.processing_word(word)
                    if self.processing_pos is not None:
                        pos = self.processing_pos(pos)
                    if self.processing_tag is not None:
                        tag = self.processing_tag(tag)

                    words += [word]
                    poss  += [pos]
                    tags  += [tag]
                    cases += [case]


    def __len__(self):
        """Iterates once over the corpus to set and store length"""
        if self.length is None:
            self.length = 0
            for _ in self:
                self.length += 1

        return self.length

def load_vocab(filename):
    """Loads vocab from a file
    Args:
        filename: (string) the format of the file must be one word per line.
    Returns:
        d: dict[word] = index
    """
    try:
        d = dict()
        with open(filename, encoding='utf-8') as f:
            for idx, word in enumerate(f):
                word = word.strip()
                d[word] = idx

    except IOError:
        raise MyIOError(filename)
    return d

def write_vocab(vocab, filename, words=True):
    """Writes a vocab to a file
    Writes one word per line.
    Args:
        vocab: iterable that yields word
        filename: path to vocab file
    Returns:
        write a word per line
    """

    print("Writing vocab...")
    with open(filename, "w", encoding='utf-8') as f:
        if words:
            f.write("{}\n".format(PAD))
            f.write("{}\n".format(EOS))
        for i, word in enumerate(vocab):
            if i != len(vocab) - 1:
                f.write("{}\n".format(word))
            else:
                f.write(word)
    print("- done. {} tokens".format(len(vocab)))

def get_vocabs(datasets):
    """Build vocabulary from an iterable of datasets objects
    Args:
        datasets: a list of dataset objects
    Returns:
        a set of all the words in the dataset
    """
    print("Building vocab...")
    vocab_words = set()
    vocab_tags = set()
    vocab_pos  = set()
    for data_ in datasets:
        for words, pos,_, tags in data_:
        #for words, tags in data_:
            vocab_words.update(words)
            vocab_tags.update(tags)
            vocab_pos.update(pos)
    print("- done. {} tokens".format(len(vocab_words)))
    return vocab_words,vocab_pos,vocab_tags
    #return vocab_words, vocab_tags

def get_vocab_documments(datasets):
    """Build vocabulary from an iterable of datasets objects
    Args:
        datasets: a list of dataset objects
    Returns:
        a set of all the words in the dataset
    """
    print("Building vocab...")
    vocab_words = set()
    vocab_tags  = set()
    for data_set in datasets:
        for text, y in data_set:
            vocab_words.update(text[0])
            vocab_tags.add(y[0])

    print("- done. {} tokens".format(len(vocab_words)))
    return vocab_words, vocab_tags

def get_char_vocab(dataset):
    """Build char vocabulary from an iterable of datasets objects
        Args:
            dataset: a iterator yielding tuples (sentence, tags)
        Returns:
            a set of all the characters in the dataset
        """
    vocab_char = set()
    for words, _,_,_ in dataset:
        # for words, _ in dataset:
        for word in words:
            for char in word:
                vocab_char.add(char)
    return vocab_char


def get_char_vocab_documents(dataset):
    """Build char vocabulary from an iterable of datasets objects
    Args:
        dataset: a iterator yielding tuples (sentence, tags)
    Returns:
        a set of all the characters in the dataset
    """
    vocab_char = set()
    for d in dataset:
        for sent,_ in d:
            for words in sent:
                for word in words:
                    for char in word:
                        vocab_char.add(char)
    return vocab_char
def get_chunk_type(tok, idx_to_tag):
    """
    Args:
        tok: id of token, ex 4
        idx_to_tag: dictionary {4: "B-PER", ...}
    Returns:
        tuple: "B", "PER"
    """
    tag_name = idx_to_tag[tok]
    tag_class = tag_name.split('-')[0]
    tag_type = tag_name.split('-')[-1]
    return tag_class, tag_type




def get_chunks_sentiment(seq, tags):
    """Given a sequence of tags, group entities and their position
    Args:
        seq: [4, 4, 0, 0, ...] sequence of labels
        tags: dict["O"] = 4
    Returns:
        list of (chunk_type, chunk_start, chunk_end)
    Example:
        seq = [4, 5, 0, 3]
        tags = {"B-PER": 4, "I-PER": 5, "B-LOC": 3}
        result = [("PER", 0, 2), ("LOC", 3, 4)]
    """
    default = tags[NONE]
    idx_to_tag = {idx: tag for tag, idx in tags.items()}
    chunks = []
    chunk_type, chunk_start = None, None
    for i, tok in enumerate(seq):
        if 1 is tok:
            tok = 1
        else:
            tok = 0
        # End of a chunk 1
        if tok == default and chunk_type is not None:
            # Add a chunk.
            chunk = (chunk_type, chunk_start, i)
            chunks.append(chunk)
            chunk_type, chunk_start = None, None

        # End of a chunk + start of a chunk!
        elif tok != default:
            tok_chunk_class, tok_chunk_type = get_chunk_type(tok, idx_to_tag)
            if chunk_type is None:
                chunk_type, chunk_start = tok_chunk_type, i
            elif tok_chunk_type != chunk_type or tok_chunk_class == "B":
                chunk = (chunk_type, chunk_start, i)
                chunks.append(chunk)
                chunk_type, chunk_start = tok_chunk_type, i
        else:
            pass

    # end condition
    if chunk_type is not None:
        chunk = (chunk_type, chunk_start, len(seq))
        chunks.append(chunk)

    return chunks








def get_chunks(seq, tags):
    """Given a sequence of tags, group entities and their position
    Args:
        seq: [4, 4, 0, 0, ...] sequence of labels
        tags: dict["O"] = 4
    Returns:
        list of (chunk_type, chunk_start, chunk_end)
    Example:
        seq = [4, 5, 0, 3]
        tags = {"B-PER": 4, "I-PER": 5, "B-LOC": 3}
        result = [("PER", 0, 2), ("LOC", 3, 4)]
    """
    default = tags[NONE]
    idx_to_tag = {idx: tag for tag, idx in tags.items()}
    chunks = []
    chunk_type, chunk_start = None, None
    for i, tok in enumerate(seq):
        # End of a chunk 1
        if tok == default and chunk_type is not None:
            # Add a chunk.
            chunk = (chunk_type, chunk_start, i)
            chunks.append(chunk)
            chunk_type, chunk_start = None, None

        # End of a chunk + start of a chunk!
        elif tok != default:
            tok_chunk_class, tok_chunk_type = get_chunk_type(tok, idx_to_tag)
            if chunk_type is None:
                chunk_type, chunk_start = tok_chunk_type, i
            elif tok_chunk_type != chunk_type or tok_chunk_class == "B":
                chunk = (chunk_type, chunk_start, i)
                chunks.append(chunk)
                chunk_type, chunk_start = tok_chunk_type, i
        else:
            pass

    # end condition
    if chunk_type is not None:
        chunk = (chunk_type, chunk_start, len(seq))
        chunks.append(chunk)

    return chunks



class UniversalDependencies(object):
    """Class that iterates over CoNLL Dataset
    __iter__ method yields a tuple (words, tags)
        words: list of raw words
        tags: list of raw tags
    If processing_word and processing_tag are not None,
    optional preprocessing is appplied
    Example:
        ```python
        data = CoNLLDataset(filename)
        for sentence, pos, labels in data:
            pass
        ```
    """
    def __init__(self, filename, processing_word=None, processing_cases=None, processing_pos=None, processing_tag=None,
                 max_iter=None):
        """
        Args:
            filename: path to the file
            processing_words: (optional) function that takes a word as input
            processing_tags: (optional) function that takes a tag as input
            max_iter: (optional) max number of sentences to yield
        """
        self.filename = filename
        self.processing_word  = processing_word
        self.processing_tag   = processing_tag
        self.processing_pos   = processing_pos
        self.processing_cases = processing_cases
        self.max_iter = max_iter
        self.length = None


    def __iter__(self):
        niter = 0
        with open(self.filename, encoding="utf-8") as f:
            words, poss, tags, cases = [], [], [], []
            for line in f:
                line = line.strip()
                if (len(line) == 0 or line.startswith("-DOCSTART-") or line.startswith("#")):
                    if len(words) != 0:
                        niter += 1
                        if self.max_iter is not None and niter > self.max_iter:
                            break
                        yield words, poss, cases, tags
                        #yield words, tags
                        words, poss, tags, cases = [], [], [], []
                else:
                    ls = line.split('\t')
                    word, pos, tag = ls[1], ls[3], ls[3]
                    if self.processing_cases is not None:
                        case = self.processing_cases(set_casing(word))
                    else:
                        case = []
                    if self.processing_word is not None:
                        word = self.processing_word(word)
                    if self.processing_pos is not None:
                        pos = self.processing_pos(pos)
                    if self.processing_tag is not None:
                        tag = self.processing_tag(tag)


                    words += [word]
                    poss  += [pos]
                    tags  += [tag]
                    cases += [case]


    def __len__(self):
        """Iterates once over the corpus to set and store length"""
        if self.length is None:
            self.length = 0
            for _ in self:
                self.length += 1

        return self.length



def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()


def load_data_and_labels_sentiment(processing_word, positive_data_file, negative_data_file, x_text, y):
    """
    Loads MR polarity data from files, splits the data into words and generates labels.
    Returns split sentences and labels.
    """
    #arrumar para colcoar um label
    # Load data from files
    positive_examples = list(open(positive_data_file, "r").readlines())
    positive_examples = [s.strip() for s in positive_examples]
    negative_examples = list(open(negative_data_file, "r").readlines())
    negative_examples = [s.strip() for s in negative_examples]
    # Split by words
    x_text.append([processing_word(clean_str(sent)) for sent in positive_examples])
    x_text.append([processing_word(clean_str(sent)) for sent in negative_examples])
    #x_text = [clean_str(sent) for sent in x_text]
    # Generate labels
    positive_labels = ["1" for _ in positive_examples]
    negative_labels = ["0" for _ in negative_examples]
    y.append(positive_labels)
    y.append(negative_labels)

class CReader(object):
    """Class that iterates over a5 Dataset
    __iter__ method yields a 3-tuple (person, description, caption/target)
        text: description
        target: binary_class
    If processing_word is not None,
    optional preprocessing is appplied
    Example:
        ```python
        data = CReader(filename)
        for text, class in data:
            pass
        ```
    """
    def __init__(self, filename, processing_text=None, processing_class=None,
                 max_iter=None):
        """
        Args:
            filename: path to the file
            processing_text: (optional) function that takes a word as input
            max_iter: (optional) max number of sentences to yield
        """
        self.filename = filename
        self.processing_text  = processing_text
        self.processing_class = processing_class
        self.max_iter = max_iter
        self.length = None


    def __iter__(self):
        niter = 0
        with open(self.filename) as f:
            text, label = [], []
            for line in f:
                line = line.strip()
                if (len(line) == 0):
                    if len(text) != 0:
                        niter += 1
                        if self.max_iter is not None and niter > self.max_iter:
                            break
                        yield text, label
                        #yield words, tags
                        text, label = [], []
                else:
                    ls = line.split('\t')
                    text_, label_ = ls[0], ls[1]
                    if self.processing_text is not None:
                        description_ = [self.processing_text(ele) for ele in text_.split()]
                    if self.processing_class is not None:
                        label_ = [self.processing_class(ele) for ele in label_.split()]
                    text  += [description_]
                    label += [label_]

    def __len__(self):
        """Iterates once over the corpus to set and store length"""
        if self.length is None:
            self.length = 0
            for _ in self:
                self.length += 1

        return self.length


