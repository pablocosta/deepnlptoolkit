import numpy as np
from utils import get_processing_word, get_embeddings, get_logger, load_vocab, create_dict_inx, transform_embeddings

class Config():
    # general config
    dir_output = "results/test/"
    dir_model  = dir_output + "model.weights/"
    path_log   = dir_output + "log.txt"

    # embeddings
    dim_word = 50
    dim_char = 50

    use_pretrained = True

    # dataset

    filename_dev   = "./data_set/valid.txt"
    filename_test  = "./data_set/test.txt"
    filename_train = "./data_set/train.txt"
    max_iter = None # if not None, max number of examples in Dataset

    # vocab (created from dataset with build_data.py)
    filename_words = "./data/words.txt"
    filename_pos   = "./data/pos.txt"
    filename_tags  = "./data/tags.txt"
    filename_chars = "./data/chars.txt"

    # training
    train_embeddings = False
    nepochs = 100
    dropout = 0.2
    batch_size = 20
    use_early  = False
    #lr_method = "1.0"
    #lr = 0.001
    #lr_decay = 0.9
    lr_method = "sgd"
    lr = 1.0
    lr_decay = 0.9
    clip = 5.0  # if negative, no clipping
    nepoch_no_imprv = 5

    # model hyperparameters
    hidden_size_encoder = 128  # lstm on word embeddings

    use_chars = True

    def __init__(self, load=True, using_bid=True):

        # create instance of logger
        self.logger = get_logger(self.dir_output + "log.txt")
        if using_bid:
            self.hidden_size_decoder = self.hidden_size_encoder * 2
        else:
            self.hidden_size_decoder = self.hidden_size_encoder
        # load if requested (default)
        if load:
            self.load()

    def load(self):
        # 1. vocabulary
        self.vocab_words   = load_vocab(self.filename_words)
        self.nwords  = len(self.vocab_words)
        self.vocab_chars = load_vocab(self.filename_chars)
        self.nchars = len(self.vocab_chars)
        self.vocab_pos   = load_vocab(self.filename_pos)
        self.vocab_tags  = load_vocab(self.filename_tags)


        # 2. get processing functions that map str -> id
        self.processing_word  = get_processing_word(vocab_words=self.vocab_words, lowercase=True, chars=self.use_chars)
        self.processing_pos   = get_processing_word(vocab_words=self.vocab_pos,   lowercase=False, chars=self.use_chars)
        self.processing_tags  = get_processing_word(vocab_words=self.vocab_tags,  lowercase=False, chars=self.use_chars)

        # 3. get pre-trained embeddings
        vocab_ = get_embeddings("pt_50.txt", 50)
        self.embeddings         = transform_embeddings(vocab_, self.vocab_words, 50)

