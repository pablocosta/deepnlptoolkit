import os

train = os.listdir(".\\data_set\\train\\")
teste = os.listdir(".\\data_set\\test\\")

train = [os.path.join(".\\data_set\\train\\", fn) for fn in train]

teste = [os.path.join(".\\data_set\\test\\", fn) for fn in teste]

files = [train, teste]
dirs  = [".\\data_set\\train\\", ".\\data_set\\test\\"]
concat_train = open(".\\data_set\\train\\out.txt", "w+", encoding="utf-8")
concat_teste = open(".\\data_set\\test\\out.txt", "w+",  encoding="utf-8")
for d,out in zip(dirs,[concat_train, concat_teste]):
    for f_ in files:
        for f in f_:
            a = open(f, "r", encoding="utf-8")
            out.write(a)