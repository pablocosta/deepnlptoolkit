import numpy as np
from utils import ROOT, NULL

def batch(inputs, max_sequence_length=None):
    """
    Args:
        inputs:
            list of sentences (integer lists)
        max_sequence_length:
            integer specifying how large should `max_time` dimension be.
            If None, maximum sequence length would be used

    Outputs:
        inputs_time_major:
            input sentences transformed into time-major matrix
            (shape [max_time, batch_size]) padded with 0s
        sequence_lengths:
            batch-sized list of integers specifying amount of active
            time steps in each input sequence
    """
    sequence_lengths = [len(seq) for seq in inputs]
    batch_size = len(inputs)

    if max_sequence_length is None:
        max_sequence_length = max(sequence_lengths)

    inputs_batch_major = np.zeros(shape=[batch_size, max_sequence_length], dtype=np.int32)  # == PAD

    for i, seq in enumerate(inputs):
        for j, element in enumerate(seq):
            inputs_batch_major[i, j] = element

    # [batch_size, max_time] -> [max_time, batch_size]
    inputs_time_major = inputs_batch_major.swapaxes(0, 1)

    return inputs_time_major, sequence_lengths


def random_sequences(length_from, length_to,
                     vocab_lower, vocab_upper,
                     batch_size):
    """ Generates batches of random integer sequences,
        sequence length in [length_from, length_to],
        vocabulary in [vocab_lower, vocab_upper]
    """
    if length_from > length_to:
        raise ValueError('length_from > length_to')

    def random_length():
        if length_from == length_to:
            return length_from
        return np.random.randint(length_from, length_to + 1)

    while True:
        yield [
            np.random.randint(low=vocab_lower,
                              high=vocab_upper,
                              size=random_length()).tolist()
            for _ in range(batch_size)
        ]




def minibatches(data, minibatch_size, config):
    """
    Args:
        sents: (word pos) tuples of lists
        sents: (head, dependency_relation) tuples of lists
        minibatch_size: (int)
    Yields:
        list of tuples: words, pos, labels, heads
    """

    words_batch, pos_batch, relation_batch, head_batch = [], [], [], []
    for tree, sent  in zip(data[0], data[1]):
        #for (x, y) in data:

        if len(words_batch) == minibatch_size:
            yield words_batch, pos_batch, relation_batch, head_batch
            #yield x_batch, y_batch
            words_batch, pos_batch, relation_batch, head_batch = [], [], [], []

        words_batch     += [sent.words]
        pos_batch       += [sent.poss]
        h = []
        r = []
        for i in range(1, sent.n + 1):
            if tree.get_head(i) == 0:
                h.append(config.processing_word(ROOT))
            else:
                h.append(sent.words[tree.get_head(i) -1])
            r.append(tree.get_label(i))

        #filtrar as palavras também... para que o "root" retorne o indice correto apartir disso adicionar o root no vetor words_batch no indice "0" e em h adicioncar "null" na mesma pocisao
        words_batch[-1].insert(0, config.processing_word(ROOT))
        h.insert(0, config.processing_word(NULL))

        relation_batch  += [r]
        head_batch      += [h]


    if len(words_batch) != 0:
        yield words_batch, pos_batch, relation_batch, head_batch
        #yield x_batch, y_batch