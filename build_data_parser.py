from configuration_parser import Config
from corpusreader import ReadSent
from utils import get_vocabs, NONE, NUM, PAD, UNK, ROOT, NULL, \
    write_vocab, get_char_vocab, \
    get_processing_word, get_embeddings

processing_word = get_processing_word(lowercase=True)
config = Config(load=False)

#create the vocabulary
train = ReadSent(path=config.filename_train, processing_word=processing_word, processing_pos=True, processing_label=True)
test  = ReadSent(path=config.filename_test,  processing_word=processing_word, processing_pos=True, processing_label=True)
dev   = ReadSent(path=config.filename_dev, processing_word=processing_word, processing_pos=True, processing_label=True)


vocab_words = set(train.vocab_words.keys()) | set(dev.vocab_words.keys())
vocab_labels = set(train.vocab_label.keys()) | set(test.vocab_label.keys()) | set(dev.vocab_label.keys())
vocab_tags   = set(train.vocab_pos.keys()) | set(test.vocab_pos.keys()) | set(dev.vocab_pos.keys())


vocab_glove = get_embeddings("pt_50.txt", 50)

vocab = vocab_words & set(vocab_glove)


vocab.add(NONE)
vocab.add(NUM)
vocab.add(BOS)
vocab.add(UNK)
vocab.add(ROOT)
vocab.add(NULL)

write_vocab(vocab, config.filename_words, True)
write_vocab(vocab_tags, config.filename_pos, False)
write_vocab(vocab_labels, config.filename_tags, False)

write_vocab(set(train.vocab_char.keys()) | set(dev.vocab_char.keys()), config.filename_chars, False)

