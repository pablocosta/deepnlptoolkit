import builda_data_pos
from utils import UniversalDependencies
from pos_tagger import POSModel
from configuration import Config

# create instance of config
config = Config()
config.load()
print(config.vocab_tags)

# build model
model = POSModel(config)
model.build()

# model.restore_session("results/crf/model.weights/") # optional, restore weights
# model.reinitialize_weights("proj")

# create datasets
train  = UniversalDependencies(config.filename_train, config.processing_word, config.processing_case, config.processing_pos, config.processing_tags)
dev    = UniversalDependencies(config.filename_dev, config.processing_word, config.processing_case, config.processing_pos, config.processing_tags)

# train model
model.train(train, dev)