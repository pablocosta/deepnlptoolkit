import numpy as np
from utils import get_processing_word, get_embeddings, get_logger, load_vocab, create_dict_inx, transform_embeddings
class Config():
    # general config
    dir_output = "results/test/"
    dir_model  = dir_output + "model.weights/"
    path_log   = dir_output + "log.txt"

    # embeddings
    dim_word = 300
    dim_char = 300
    dim_pos  = 100

    use_pretrained = True

    # dataset
    # filename_dev = "data/coNLL/eng/eng.testa.iob"
    # filename_test = "data/coNLL/eng/eng.testb.iob"
    # filename_train = "data/coNLL/eng/eng.train.iob"

    filename_dev   = "./data_set/valid.txt"
    filename_train = "./data_set/train.txt"
    filename_test  = "./data_set/test.txt"

    #filename_dev   = "./data_set/valid.txt"
    #filename_train = "./data_set/train.txt"
    #filename_test  = "./data_set/test.txt"
    max_iter = None # if not None, max number of examples in Dataset

    # vocab (created from dataset with build_data.py)
    filename_words = "data/words.txt"
    filename_tags = "data/tags.txt"
    filename_pos = "data/pos.txt"
    filename_chars = "data/chars.txt"

    # training
    train_embeddings = False
    nepochs = 300
    dropout = 0.5
    batch_size = 20
    lr_method = "adam"
    lr = 0.001
    lr_decay = 0.9
    #lr_method = "sgd"
    #lr = 0.01
    #lr_decay = 0.9
    #clip = 5.0  # if negative, no clipping
    clip = -1  # if negative, no clipping
    nepoch_no_imprv = 5


    # model hyperparameters
    #hidden_size_char = 100 # lstm on chars
    hidden_size_char = 100  # lstm on chars

    hidden_size_lstm = 100 # lstm on word embeddings
    #hidden_size_lstm = 100  # lstm on word embeddings

    # NOTE: if both chars and crf, only 1.6x slower on GPU
    use_crf   = True # if crf, training is 1.7x slower on CPU
    use_chars = True # if char embedding, training is 3.5x slower on CPU
    use_pos   = False # not implemented
    use_case  = False # on implementation

    def __init__(self, load=True):

        # create instance of logger
        self.logger = get_logger(self.dir_output + "log.txt")

        # load if requested (default)
        if load:
            self.load()

    def load(self):
        # 1. vocabulary
        self.vocab_words = load_vocab(self.filename_words)
        self.vocab_tags = load_vocab(self.filename_tags)
        self.vocab_pos = load_vocab(self.filename_pos)
        self.vocab_chars = load_vocab(self.filename_chars)
        #self.vocab_chars = reader.vocab_char
        self.nwords  = len(self.vocab_words)
        self.nchars  = len(self.vocab_chars)
        #print(self.nchars)
        print(self.vocab_tags)
        self.ntags = len(self.vocab_tags)
        self.npos    = len(self.vocab_pos)
        case2Idx = {'numeric': 0, 'allLower': 1, 'allUpper': 2, 'initialUpper': 3, 'other': 4, 'mainly_numeric': 5,
                    'contains_digit': 6, 'PADDING_TOKEN': 7}



        # 2. get processing functions that map str -> id
        self.processing_word  = get_processing_word(self.vocab_words, self.vocab_chars, lowercase=True, chars=self.use_chars)
        self.processing_pos   = get_processing_word(self.vocab_pos,
                lowercase=False, allow_unk=False)
        self.processing_tags = get_processing_word(self.vocab_tags,
                lowercase=False, allow_unk=True)
        self.processing_case = get_processing_word(case2Idx, lowercase=False, allow_unk=False)

        # 3. get pre-trained embeddings
        vocab_glove_ = get_embeddings("pt_50.txt", 50)
        self.embeddings = transform_embeddings(vocab_glove_, self.vocab_words, 50)
        self.caseEmbeddings = np.identity(len(case2Idx), dtype='int32')




