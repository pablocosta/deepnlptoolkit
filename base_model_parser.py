import math
import helpers
import numpy as np
import tensorflow as tf
import tensorflow.contrib.seq2seq as seq2seq

from tensorflow.contrib.layers import safe_embedding_lookup_sparse as embedding_lookup_unique
from tensorflow.contrib.rnn import LSTMCell, LSTMStateTuple, GRUCell
from utils import EOS, PAD, Progbar, pad_sentences


class Seq2SeqModel():
    """Seq2Seq model usign blocks from new `tf.contrib.seq2seq`.
    Requires TF 1.0.0-alpha"""

    def __init__(self, config, encoder_cell, decoder_cell, vocab_size, embedding_size,
                 bidirectional=True,
                 attention=False,
                 debug=False,
                 attention_form="bahdanau"):
        self.debug = debug
        self.bidirectional = bidirectional
        self.attention = attention
        self.attention_form = attention_form

        self.vocab_size = vocab_size
        self.embedding_size = embedding_size
        self.config = config
        self.encoder_cell = encoder_cell
        self.decoder_cell = decoder_cell
        self.PAD = config.vocab_words[PAD]
        self.EOS = config.vocab_words[EOS]
        self._make_graph()

    @property
    def decoder_hidden_units(self):
        # @TODO: is this correct for LSTMStateTuple?
        return self.decoder_cell.output_size

    def _make_graph(self):
        if self.debug:
            self._init_debug_inputs()
        else:
            self._init_placeholders()

        self._init_decoder_train_connectors()
        self._init_embeddings()

        if self.bidirectional:
            self._init_bidirectional_encoder()
        else:
            self._init_simple_encoder()

        self._init_decoder()

        self._init_optimizer()

    def _init_debug_inputs(self):
        """ Everything is time-major """
        x = [[5, 6, 7],
             [7, 6, 0],
             [0, 7, 0]]
        xl = [2, 3, 1]
        self.encoder_inputs = tf.constant(x, dtype=tf.int32, name='encoder_inputs')
        self.encoder_inputs_length = tf.constant(xl, dtype=tf.int32, name='encoder_inputs_length')

        self.decoder_targets = tf.constant(x, dtype=tf.int32, name='decoder_targets')
        self.decoder_targets_length = tf.constant(xl, dtype=tf.int32, name='decoder_targets_length')

    def _init_placeholders(self):
        """ Everything is time-major """
        self.encoder_inputs = tf.placeholder(
            shape=[None, None],
            dtype=tf.int32,
            name='encoder_inputs',
        )
        self.encoder_inputs_length = tf.placeholder(
            shape=[None,],
            dtype=tf.int32,
            name='encoder_inputs_length',
        )

        # required for training, not required for testing
        self.decoder_targets = tf.placeholder(
            shape=[None, None],
            dtype=tf.int32,
            name='decoder_targets'
        )
        self.decoder_targets_length = tf.placeholder(
            shape=[None,],
            dtype=tf.int32,
            name='decoder_targets_length',
        )

    def _init_decoder_train_connectors(self):
        """
        During training, `decoder_targets`
        and decoder logits. This means that their shapes should be compatible.
        Here we do a bit of plumbing to set this up.
        """
        with tf.name_scope('DecoderTrainFeeds'):
            sequence_size, batch_size = tf.unstack(tf.shape(self.decoder_targets))

            EOS_SLICE = tf.ones([1, batch_size], dtype=tf.int32) * self.EOS
            PAD_SLICE = tf.ones([1, batch_size], dtype=tf.int32) * self.PAD

            self.decoder_train_inputs = tf.concat([EOS_SLICE, self.decoder_targets], axis=0)
            self.decoder_train_length = self.decoder_targets_length + 1

            decoder_train_targets = tf.concat([self.decoder_targets, PAD_SLICE], axis=0)
            decoder_train_targets_seq_len, _ = tf.unstack(tf.shape(decoder_train_targets))
            decoder_train_targets_eos_mask = tf.one_hot(self.decoder_train_length - 1,
                                                        decoder_train_targets_seq_len,
                                                        on_value=self.EOS, off_value=self.PAD,
                                                        dtype=tf.int32)
            decoder_train_targets_eos_mask = tf.transpose(decoder_train_targets_eos_mask, [1, 0])

            # hacky way using one_hot to put EOS symbol at the end of target sequence
            decoder_train_targets = tf.add(decoder_train_targets,
                                           decoder_train_targets_eos_mask)

            self.decoder_train_targets = decoder_train_targets

            self.loss_weights = tf.ones([
                batch_size,
                tf.reduce_max(self.decoder_train_length)
            ], dtype=tf.float32, name="loss_weights")

    def _init_embeddings(self):
        with tf.variable_scope("embedding") as scope:


            if self.config.use_pretrained:
                self.embedding_matrix = tf.Variable(
                    self.config.embeddings,
                    name="embedding_matrix",
                    dtype=tf.float32,
                    trainable=self.config.train_embeddings)
            else:
                # Uniform(-sqrt(3), sqrt(3)) has variance=1.
                sqrt3 = math.sqrt(3)
                initializer = tf.random_uniform_initializer(-sqrt3, sqrt3)
                self.embedding_matrix = tf.get_variable(
                    name="embedding_matrix",
                    shape=[self.vocab_size, self.embedding_size],
                    initializer=initializer,
                    dtype=tf.float32)

            self.encoder_inputs_embedded = tf.nn.embedding_lookup(
                self.embedding_matrix, self.encoder_inputs)

            self.decoder_train_inputs_embedded = tf.nn.embedding_lookup(
                self.embedding_matrix, self.decoder_train_inputs)

    def _init_simple_encoder(self):
        with tf.variable_scope("Encoder") as scope:
            (self.encoder_outputs, self.encoder_state) = (
                tf.nn.dynamic_rnn(cell=self.encoder_cell,
                                  inputs=self.encoder_inputs_embedded,
                                  sequence_length=self.encoder_inputs_length,
                                  time_major=True,
                                  dtype=tf.float32)
                )

    def _init_bidirectional_encoder(self):
        with tf.variable_scope("BidirectionalEncoder") as scope:

            ((encoder_fw_outputs,
              encoder_bw_outputs),
             (encoder_fw_state,
              encoder_bw_state)) = (
                tf.nn.bidirectional_dynamic_rnn(cell_fw=self.encoder_cell,
                                                cell_bw=self.encoder_cell,
                                                inputs=self.encoder_inputs_embedded,
                                                sequence_length=self.encoder_inputs_length,
                                                time_major=True,
                                                dtype=tf.float32)
                )

            self.encoder_outputs = tf.concat((encoder_fw_outputs, encoder_bw_outputs), 2)
            self.encoder_outputs = tf.nn.dropout(self.encoder_outputs, self.config.dropout)

            if isinstance(encoder_fw_state, LSTMStateTuple):

                encoder_state_c = tf.concat(
                    (encoder_fw_state.c, encoder_bw_state.c), 1, name='bidirectional_concat_c')
                encoder_state_h = tf.concat(
                    (encoder_fw_state.h, encoder_bw_state.h), 1, name='bidirectional_concat_h')
                self.encoder_state = LSTMStateTuple(c=encoder_state_c, h=encoder_state_h)

            elif isinstance(encoder_fw_state, tf.Tensor):
                self.encoder_state = tf.concat((encoder_fw_state, encoder_bw_state), 1, name='bidirectional_concat')




    def run_epoch(self, train, dev, model, session, epoch):
        """Performs one complete pass over the train set and evaluate on dev
        Args:
            train: dataset that yields tuple of sentences, tags
            dev: dataset
            epoch: (int) index of the current epoch
        Returns:
            f1: (python float), score to select model on, higher is better
        """
        ######aplicar as funções de recuperação e mapeamento de indice diretamente no depedencyTree
        # progbar stuff for logging
        batch_size = self.config.batch_size
        nbatches = (len(train[0]) + batch_size - 1) // batch_size
        prog = Progbar(target=nbatches)
        metrics = dict()
        batch_x =  [batch_x for (batch_x, _, _, _) in helpers.minibatches(train, self.config.batch_size, self.config)]
        batch_y = [batch_y for (_, _, _ , batch_y) in helpers.minibatches(train, self.config.batch_size, self.config)]

        # iterate over dataset
        for i, (x, y) in enumerate(zip(batch_x, batch_y)):

            fd = self.make_train_inputs(x, y)
            _,train_loss = session.run([model.train_op, model.loss], fd)
            prog.update(i + 1, [("train loss", train_loss)])

        metrics["mean_loss"], track_loss = self.run_evaluate(dev, session, model)
        msg = " - ".join(["{} {:04.12f}".format(k, v)
                for k, v in metrics.items()])
        print(msg)




        return metrics["mean_loss"], track_loss


    def run_evaluate(self, dev, session, model):
        batch_x = [batch_x for (batch_x, _, _, _) in helpers.minibatches(dev, self.config.batch_size, self.config)]
        batch_y = [batch_y for (_, _, _, batch_y) in helpers.minibatches(dev, self.config.batch_size, self.config)]
        m_loss = []
        for i, (x,y) in enumerate(zip(batch_x, batch_y)):
            fd = self.make_train_inputs(x, y)
            _, loss = session.run([model.train_op, model.loss], fd)
            m_loss.append(loss)

        return np.mean(m_loss), m_loss

    def train(self, train, dev, session, model):
        best_score = np.iinfo(np.int32(10)).max
        nepoch_no_imprv = 0  # for early stopping
        track_loss = []
        for epoch in range(self.config.nepochs):
            print("Epoch {:} out of {:}".format(epoch + 1,
                                                           self.config.nepochs))

            score, loss = self.run_epoch(train, dev, model, session, epoch)
            self.config.lr *= self.config.lr_decay  # decay learning rate
            track_loss.extend(loss)
            # early stopping and saving best parameters
            if score <= best_score:
                nepoch_no_imprv = 0
                best_score = score
                print("- new best score!")

            else:
                nepoch_no_imprv += 1
                if nepoch_no_imprv >= self.config.nepoch_no_imprv:
                    print("- early stopping {} epochs without " \
                                     "improvement".format(nepoch_no_imprv))
                    if self.config.use_early:
                        break
                    else:
                        pass
        return track_loss




    def _init_decoder(self):
        with tf.variable_scope("Decoder") as scope:
            def output_fn(outputs):
                return tf.contrib.layers.linear(outputs, self.vocab_size, scope=scope)

            if not self.attention:
                decoder_fn_train = seq2seq.simple_decoder_fn_train(encoder_state=self.encoder_state)
                decoder_fn_inference = seq2seq.simple_decoder_fn_inference(
                    output_fn=output_fn,
                    encoder_state=self.encoder_state,
                    embeddings=self.embedding_matrix,
                    start_of_sequence_id=self.EOS,
                    end_of_sequence_id=self.EOS,
                    maximum_length=tf.reduce_max(self.encoder_inputs_length) + 3,
                    num_decoder_symbols=self.vocab_size,
                )
            else:

                # attention_states: size [batch_size, max_time, num_units]
                attention_states = tf.transpose(self.encoder_outputs, [1, 0, 2])

                (attention_keys,
                attention_values,
                attention_score_fn,
                attention_construct_fn) = seq2seq.prepare_attention(
                    attention_states=attention_states,
                    attention_option=self.attention_form,
                    num_units=self.decoder_hidden_units,
                )

                decoder_fn_train = seq2seq.attention_decoder_fn_train(
                    encoder_state=self.encoder_state,
                    attention_keys=attention_keys,
                    attention_values=attention_values,
                    attention_score_fn=attention_score_fn,
                    attention_construct_fn=attention_construct_fn,
                    name='attention_decoder'
                )

                decoder_fn_inference = seq2seq.attention_decoder_fn_inference(
                    output_fn=output_fn,
                    encoder_state=self.encoder_state,
                    attention_keys=attention_keys,
                    attention_values=attention_values,
                    attention_score_fn=attention_score_fn,
                    attention_construct_fn=attention_construct_fn,
                    embeddings=self.embedding_matrix,
                    start_of_sequence_id=self.EOS,
                    end_of_sequence_id=self.EOS,
                    maximum_length=tf.reduce_max(self.encoder_inputs_length) + 3,
                    num_decoder_symbols=self.vocab_size,
                )

            (self.decoder_outputs_train,
             self.decoder_state_train,
             self.decoder_context_state_train) = (
                seq2seq.dynamic_rnn_decoder(
                    cell=self.decoder_cell,
                    decoder_fn=decoder_fn_train,
                    inputs=self.decoder_train_inputs_embedded,
                    sequence_length=self.decoder_train_length,
                    time_major=True,
                    scope=scope,
                )
            )

            self.decoder_logits_train = output_fn(self.decoder_outputs_train)
            self.decoder_prediction_train = tf.argmax(self.decoder_logits_train, axis=-1, name='decoder_prediction_train')

            scope.reuse_variables()

            (self.decoder_logits_inference,
             self.decoder_state_inference,
             self.decoder_context_state_inference) = (
                seq2seq.dynamic_rnn_decoder(
                    cell=self.decoder_cell,
                    decoder_fn=decoder_fn_inference,
                    time_major=True,
                    scope=scope,
                )
            )
            self.decoder_prediction_inference = tf.argmax(self.decoder_logits_inference, axis=-1, name='decoder_prediction_inference')

    def _init_optimizer(self):
        logits = tf.transpose(self.decoder_logits_train, [1, 0, 2])
        targets = tf.transpose(self.decoder_train_targets, [1, 0])
        ##########################################aquiii tenho que ver com o codigo do git
        self.loss = seq2seq.sequence_loss(logits=logits, targets=targets, weights=self.loss_weights)
        ###_init_optimizer
        if self.config.lr_method == 'adam':  # sgd method
            optimizer = tf.train.AdamOptimizer(self.config.lr)
        elif self.config.lr_method == 'adagrad':
            optimizer = tf.train.AdagradOptimizer(self.config.lr)
        elif self.config.lr_method == 'sgd':
            optimizer = tf.train.GradientDescentOptimizer(self.config.lr)
        elif self.config.lr_method == 'rmsprop':
            optimizer = tf.train.RMSPropOptimizer(self.config.lr)
        elif self.config.lr_method == "nadam":
            optimizer = tf.contrib.opt.NadamOptimizer(self.config.lr)
        else:
            raise NotImplementedError("Unknown method {}".format(self.config.lr_method))

        if self.config.clip > 0:  # gradient clipping if clip is positive
            grads, vs = zip(*optimizer.compute_gradients(self.loss))
            grads, gnorm = tf.clip_by_global_norm(grads, self.config.clip)
            self.train_op = optimizer.apply_gradients(zip(grads, vs))
        else:
            self.train_op = optimizer.minimize(self.loss)
        #logits = tf.transpose(self.decoder_logits_train, [1, 0, 2])
        #targets = tf.transpose(self.decoder_train_targets, [1, 0])
        #self.loss = seq2seq.sequence_loss(logits=logits, targets=targets,
                                          #weights=self.loss_weights)
        #self.train_op = tf.train.AdamOptimizer().minimize(self.loss)

    def make_train_inputs(self, input_seq, target_seq):
        inputs_, inputs_length_ = helpers.batch(input_seq)
        targets_, targets_length_ = helpers.batch(target_seq)
        return {
            self.encoder_inputs: inputs_,
            self.encoder_inputs_length: inputs_length_,
            self.decoder_targets: targets_,
            self.decoder_targets_length: targets_length_,
        }

    def make_inference_inputs(self, input_seq):
        inputs_, inputs_length_ = helpers.batch(input_seq)
        return {
            self.encoder_inputs: inputs_,
            self.encoder_inputs_length: inputs_length_,
        }