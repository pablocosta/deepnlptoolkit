import build_data
from utils import CoNLLDataset
from ner_tagger import NERModel
from configuration import Config

# create instance of config
config = Config()
config.load()
# build model
model = NERModel(config)
model.build()
# model.restore_session("results/crf/model.weights/") # optional, restore weights
# model.reinitialize_weights("proj")

# create datasets
#train  = CoNLLDataset(config.filename_train, config.processing_word, config.processing_tags)
train  = CoNLLDataset(config.filename_train, config.processing_word, config.processing_case, config.processing_pos, config.processing_tags)

dev    = CoNLLDataset(config.filename_dev, config.processing_word, config.processing_case, config.processing_pos, config.processing_tags)
#dev    = CoNLLDataset(config.filename_dev, config.processing_word, config.processing_tags)



# train model
model.train(train, dev)


"""
https://github.com/kamalkraj/Named-Entity-Recognition-with-Bidirectional-LSTM-CNNs/blob/master/nn.py novas features


https://arxiv.org/pdf/1505.05008.pdf - artigo do cicero

new features to the model
def getCasing(word):   
    casing = 'other'

    numDigits = 0
    for char in word:
        if char.isdigit():
            numDigits += 1

    digitFraction = numDigits / float(len(word))

    if word.isdigit(): #Is a digit
        casing = 'numeric'
    elif digitFraction > 0.5:
        casing = 'mainly_numeric'
    elif word.islower(): #All lower case
        casing = 'allLower'
    elif word.isupper(): #All upper case
        casing = 'allUpper'
    elif word[0].isupper(): #is a title, initial char upper, then all lower
        casing = 'initialUpper'
    elif numDigits > 0:
        casing = 'contains_digit'
https://github.com/teropa/nlp/tree/master/resources/corpora/conll2002
https://github.com/teropa/nlp/tree/master/resources/corpora/conll2002
https://github.com/teropa/nlp/tree/master/resources/corpora/conll2002

    return casing
"""